import requests
import dateutil.parser
import datetime
import os
import time
import re
import decimal
from collections import OrderedDict, defaultdict, Counter
from app import app
from flask import render_template, request, Response, session, redirect, make_response
from functools import wraps
import ConfigParser
import random
import logging
import grequests
import ast

from app.lib import intel

fh = logging.FileHandler('error.log')
app.logger.addHandler(fh)

# Settings
#
settings = ConfigParser.ConfigParser()
settings.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'conf', 'config.cfg'))

SHORT_NAME_MAP = {'estimatedCurrentOffense': 'Practical Off',
                  'estimatedCurrentDefense': 'Estimated Def',
                  'drafted': 'Draft',
                  'maxPopulation': 'Max Pop',
                  'maxOffense': 'Max Off',
                  'minDefense': 'Min Def',
                  'offSpecs': 'O Specs',
                  'defSpecs': 'D Specs',
                  'military Barracks': 'Rax',
                  'guard Stations': 'GS',
                  'watch Towers': 'WT',
                  'modDefense': 'Mod Def',
                  'modOffense': 'Mod Off',
                  'buildingEfficiency': 'BE',
                  'tradeBalance': 'TB',
                  }


def ConfigSectionMap(section):
    dict1 = {}
    options = settings.options(section)
    for option in options:
        try:
            dict1[option] = settings.get(section, option)
            if dict1[option] == -1:
                print("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dict1[option] = None
    return dict1


def get_self_kingdom_info():

    kd_data = fetch(['kingdoms/'])
    kdl = kd_data[0]['id'], kd_data[0]['id']

    return kdl

#Secret key
app.secret_key = os.urandom(24)
# LucidBot
lucid = ConfigSectionMap("Settings")['lucidbot api path']
# Kingdom ID and Location
kdl = ConfigSectionMap("Settings")['kingdom id'], ConfigSectionMap("Settings")['kingdom location']
# if kdl == "Auto":
#     try:
#         kdl = get_self_kingdom_info()
#     except Exception:
#         kdl = 1, '(1:1)'
#         app.logger.exception("Failed to automatically determine kd")
# Kingdom Cache
Cache_Kingdom = settings.getboolean("Settings", 'kingdom cache')
#Quote of the day
qotd = settings.getboolean("Settings", 'quote of the day')
cache = {}


# Helpers
#
# Auth
def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not requests.get(lucid, auth=(auth.username, auth.password)).status_code == 200:
            return Response('Access Denied..', 401, {'WWW-Authenticate': 'Basic realm="Login"'})
        return f(*args, **kwargs)

    return decorated


def get_intel_id(data, type):
    id = '0'
    try:
        id = str(data[type]['id'])
    except Exception:
        pass

    return id

def grequest_exception_handler(request, exception):
    app.logger.exception('Error request {}. Exception: {}'.format(request, exception))

#
# Fetch
def fetch(req, pack='', method='get'):
    headers = {'content-type': 'text/plain', 'Accept': 'application/json'}
    if len(pack) > 0 or method == 'post':
        return requests.post(lucid + req, data=pack,
                             auth=(request.authorization.username, request.authorization.password)).status_code
    elif method == 'put':
        headers = {'content-type': 'application/json', 'Accept': 'application/json'}
        return (grequests.put(lucid + r, data=pack, headers=headers,
                             auth=(request.authorization.username, request.authorization.password)) for r in req)
    else:
        rs = (grequests.get(lucid + r, headers=headers,
                            auth=(request.authorization.username, request.authorization.password)) for r in req)

        return grequests.map(rs)

#
# Navbar
def navdata(kingdoms):
    kd_links = []
    for kingdom in kingdoms:
        updated_time = datetime.datetime.now() - dateutil.parser.parse(kingdom['lastUpdated'][:-5])
        if int(kdl[0]) != int(kingdom['id']):
            kd_links.append((kingdom['id'], kingdom['location'], updated_time))

    kd_links = sorted(kd_links, key=lambda kd: kd[2])
    if request.cookies.get('pin'):
        pinned_kingdom = ast.literal_eval(request.cookies.get('pin'))
    else:
        pinned_kingdom = None
    return kd_links, pinned_kingdom


def organizeHeaders(keys, headers):

    headdict = {}
    if 'submit' in keys:
        keys.remove('submit')
    i = 0
    for header in headers:
        x = {header: (i, '')}
        if header in keys:
            x = {header: (int(i), 'checked')}
        i += 3
        headdict.update(x)
    sorted_header = OrderedDict(sorted(headdict.items(), key=lambda x: x[1]))
    raw_headers = headdict

    return sorted_header, raw_headers


def getSpellOpsHeader(method, page, keys=None, default_header=None):

    raw_headers = None

    default_headers = OrderedDict(
        [('Province', (0, 'checked')), ('Race', (5, 'checked')), ('Persona', (10, 'checked')), ('Acres', (15, 'checked')),
         ('Networth', (20, 'checked')), ('Fireball', (25, 'checked')), ('Nightmare', (30, 'checked')), ('Tornadoes', (35, 'checked')),
         ('Lighnting Strike', (40, 'checked')), ('Land Lust', (45, 'checked')), ('Night Strike', (50, 'checked')), ('Kidnap', (55, 'checked')),
         ('Assassinate Wizards', (60, 'checked')), ('Rob Vaults', (65, 'checked')), ('Rob Towers', (70, 'checked')),
         ('Rob Granaries', (75, 'checked'))])

    if default_header:
        return default_headers, raw_headers

    headers = (
        'Province', 'Race', 'Persona', 'Acres', 'Networth', 'Fireball', 'Nightmare', 'Tornadoes', 'Lighnting Strike',
        'Land Lust', 'Night Strike', 'Kidnap', 'Assassinate Wizards', 'Rob Vaults', 'Rob Towers', 'Rob Granaries')
    if method == 'POST' and keys:
        sorted_header, raw_headers = organizeHeaders(keys, headers)
    else:
        try:
            head = request.cookies.get(page)
            if not head:
                sorted_header = default_headers
            else:
                headdict = ast.literal_eval(head)
                sorted_header = OrderedDict(sorted(headdict.items(), key=lambda x: x[1]))
        except Exception as e:
            sorted_header = default_headers
            app.logger.exception(e)

    return sorted_header, raw_headers


def getKingdomHeader(method, page, keys=None, default_header=None):

    raw_headers = None
    #Kingdom Header Stuff
    default_headers = OrderedDict(
    [('Owner', (0, '')), ('Race', (2, 'checked')), ('Pers', (4, 'checked')), ('Acres', (6, 'checked')), ('Networth', (9, 'checked')),
     ('Gold', (12, 'checked')), ('Food', (15, 'checked')), ('Runes', (18, 'checked')), ('Peasants', (21, 'checked')),
     ('Horses', (24, '')), ('Soldiers', (27, 'checked')), ('O Specs', (30, '')), ('D Specs', (33, 'checked')),
     ('Elites', (36, 'checked')),  ('WPA', (39, 'checked')), ('MWPA', (40, 'checked')), ('TPA', (42, 'checked')), ('MTPA', (43, 'checked')),
     ('Wizards', (44, '')), ('Thieves', (45, '')), ('PPA', (46, '')), ('BE', (48, '')), ('Books', (50, '')), ('BPA', (51, '')), ('Unallocated Books', (52, '')),
     ('NWPA', (54, '')), ('OPA', (57, '')), ('DPA', (60, '')),
     ('Mod Off', (63, 'checked')), ('SoM Off', (65, '')), ('Practical Off', (67, '')), ('Max Off', (69, '')),
     ('Mod Def', (71, '')), ('SoM Def', (74, '')), ('Estimated Def', (76, 'checked')), ('Min Def', (80, '')),
     ('Banks', (82, '')), ('GS', (84, '')), ('WT', (86, '')), ('Rax', (88, '')), ('Towers', (90, '')),
     ('Inc Land', (93, '')), ('Armies', (95, '')), ('Hit', (97, '')), ('Elites in Training', (98, '')), ('OSpecs in Training', (101, '')), ('DSpecs in Training', (103, '')),
     ('Elites Home', (110, '')), ('OME', (115, '')), ('DME', (120, '')), ('Alchemy', (130, '')), ('Tools', (140, '')), ('Housing', (150, '')), ('Production', (160, '')),
     ('Military', (170, '')), ('Crime', (180, '')), ('Channeling', (190, '')), ('Spec Creds', (200, '')), ('Wage Rate', (205, '')), ('Draft Target', (210, '')),
     ('Draft Rate', (215, ''))])

    if default_header:
        return default_headers, raw_headers

    headers = (
        'Owner', 'Race', 'Pers', 'Acres', 'Networth', 'Gold', 'Food', 'Runes', 'Peasants', 'Horses', 'Soldiers', 'O Specs', 'D Specs',
        'Elites', 'WPA', 'MWPA', 'TPA', 'MTPA', 'Wizards', 'Thieves', 'PPA', 'BE', 'Books', 'BPA', 'Unallocated Books', 'NWPA', 'OPA', 'DPA', 'Mod Off', 'SoM Off', 'Practical Off',
        'Max Off', 'Mod Def', 'SoM Def', 'Estimated Def', 'Min Def', 'Banks', 'GS', 'WT', 'Rax', 'Towers', 'Inc Land', 'Armies', 'Hit', 'Elites in Training',
        'OSpecs in Training', 'DSpecs in Training', 'Elites Home', 'OME', 'DME', 'Alchemy', 'Tools', 'Housing', 'Production', 'Military', 'Crime', 'Channeling',
        'Spec Creds', 'Wage Rate', 'Draft Target', 'Draft Rate')

    if method == 'POST' and keys:
        sorted_header, raw_headers = organizeHeaders(keys, headers)
    else:
        try:
            head = request.cookies.get(page)
            if not head:
                sorted_header = default_headers
            else:
                headdict = ast.literal_eval(head)
                sorted_header = OrderedDict(sorted(headdict.items(), key=lambda x: x[1]))
        except Exception as e:
            sorted_header = default_headers
            app.logger.exception(e)

    return sorted_header, raw_headers
# Pages
#
# Index
@app.route('/')
@requires_auth
def index():
    #user_info = fetch('users?nick={}'.format(request.authorization.username))[0]

    api_data = fetch(['weblinks', 'orders', 'events', 'aid', 'users/spelloptargets', 'quotes', 'kingdoms'])

    links = api_data[0].json()
    kd_links, kdp = navdata(api_data[6].json())
    main = {'title': 'HAL', 'kdb': kd_links, 'kdl': kdl, 'kdp': kdp}

    main['spellop_target'] = {'target': {'name': ''}}
    #links
    main['links'] = links
    try:
        main['orders'] = api_data[1].json()
        main['events'] = api_data[2].json()
        main['aid'] = []
        for aid in api_data[3].json():
            if aid['importance'] != 'Offering Aid':
                main['aid'].append(aid)

    except Exception:
        app.logger.exception('Error during orders/events/aid data gathering for index page')
    try:
        for spelloptarget in api_data[4].json():
            if spelloptarget['user']['name'] == str(request.authorization.username):
                main['spellop_target'] = spelloptarget
    except Exception:
        main['spellop_target']['target']['name'] = 'No Target'

    #Quote of the day(really quote of the page load)
    if qotd is True:
        try:
            quotes = api_data[5].json()
            count = len(quotes)
            main['quote'] = quotes[random.randrange(0, count)]

        except Exception:
            app.logger.exception('Error during quote data gathering for index page')
    return render_template('index.html', data=main, s=session)

#
# Add Intel
@app.route('/addintel', methods=['POST'])
@requires_auth
def addintel():
    form_data = request.form['intel'].encode('ascii', 'ignore')

    if 'Current kingdom is ' in form_data or 'Kingdom Analysis' in form_data:
        p = fetch('kingdoms', form_data)
        inteltype = 'Kingdom Page'
    elif 'Your forces arrive at ' in form_data:
        p = fetch('attacking', form_data)
        inteltype = 'Attack'
    elif 'Early indications show that our operation was a success.' in form_data:
        p = fetch('spells_ops', form_data)
        inteltype = 'Thief Op'
    elif 'Your wizards gather ' in form_data:
        p = fetch('spells_ops', form_data)
        inteltype = 'Spell'
    elif 'The Kingdom Reporter' in form_data or "Our thieves have stolen the last 2 month's of kingdom news from" in form_data:
        p = fetch('news', form_data)
        inteltype = 'News'
    else:
        p = fetch('intel', form_data)
        inteltype = 'Intel'
    if p == 200:
        session['intel'] = 'Successfully Posted {}'.format(inteltype)
    elif p == 400:
        session['intel'] = 'Bad {} or Unable to parse'.format(inteltype)
    elif p == 500:
        session['intel'] = 'Something went wrong posting {}'.format(inteltype)
    else:
        session['intel'] = 'Failed to post {}. Error code:{}'.format(inteltype, p)
    return redirect('/')

#
# Search
@app.route('/search', methods=['POST'])
@requires_auth
def search():
    kd_formatted = ''
    api_data = fetch(['kingdoms', 'provinces'])

    form_data = request.form['search'].encode('ascii', 'ignore')
    kds = {i['location']: i['id'] for i in api_data[0].json()}
    provs = {i['name']: i['id'] for i in api_data[1].json()}

    if ":" in form_data:
        kd_formatted = '({0})'.format(re.sub('()', '', form_data))
    try:
        if kd_formatted in kds:
            return redirect('/kd/' + str(kds[kd_formatted]))
        elif request.form['search'] in provs:
            return redirect('/intel/' + str(provs[form_data]))
        else:
            return redirect('/')
    except Exception:
        app.logger.exception("Error trying to locate kingdom")
    return redirect('/')
#
# Province
@app.route('/intel/<int:pid>')
@requires_auth
def province(pid):
    api_data = fetch(['provinces/' + str(pid), 'kingdoms'])

    p = api_data[0].json()
    kingdoms = api_data[1].json()

    intel_data = fetch(['intel/sots/' + get_intel_id(p, 'sot'), 'intel/soms/' + get_intel_id(p, 'som'),
                        'intel/soss/' + get_intel_id(p, 'sos'), 'intel/surveys/' + get_intel_id(p, 'survey')])

    kd_links, kdp = navdata(kingdoms)
    updated = {}
    main = {'p': p, 'Type': 'Province', 'pid': pid, 'title': p['name'], 'kdb': kd_links, 'kdl': kdl, 'kdp': kdp}

    try:
        main['t'] = intel_data[0].json()
        # -> Time
        t = str(datetime.datetime.now() - dateutil.parser.parse(main['t']['lastUpdated'][:-5]))
        t = t[:t.find('.')]
        if 'day' not in t:
            updated['sot'] = 'success', t[:t.find(',')]
        else:
            updated['sot'] = 'danger', t[:t.find(',')]
    except:
        pass
    try:
        main['m'] = intel_data[1].json()
        # -> Time
        t = str(datetime.datetime.now() - dateutil.parser.parse(main['m']['lastUpdated'][:-5]))
        t = t[:t.find('.')]
        if 'day' not in t:
            updated['som'] = 'success', t[:t.find(',')]
        else:
            updated['som'] = 'danger', t[:t.find(',')]
    except:
        pass
    try:
        main['s'] = intel_data[2].json()
        # -> Time
        t = str(datetime.datetime.now() - dateutil.parser.parse(main['s']['lastUpdated'][:-5]))
        t = t[:t.find('.')]
        if 'day' not in t:
            updated['sos'] = 'success', t[:t.find(',')]
        else:
            updated['sos'] = 'danger', t[:t.find(',')]
    except:
        pass
    try:
        main['su'] = intel_data[3].json()
        main['su']['total'] = int(main['su']['inProgress']) + int(main['su']['built'])
        # -> Time
        t = str(datetime.datetime.now() - dateutil.parser.parse(main['su']['lastUpdated'][:-5]))
        t = t[:t.find('.')]
        if 'day' not in t:
            updated['su'] = 'success', t[:t.find(',')]
        else:
            updated['su'] = 'danger', t[:t.find(',')]
    except:
        pass
    main['updated'] = updated
    main['kd_links'] = kd_links
    return render_template('province.html', data=main)

#
# Kingdom
@app.route('/kd/<int:kid>', methods=['GET', 'POST'])
@requires_auth
def kingdom(kid):

    api_data = fetch(['kingdoms/{}?includeIntel=true'.format(kid), 'spells/duration?kingdomIds={}'.format(kid),
                      'ops/duration?kingdomIds={}'.format(kid), 'kingdoms'])
    try:
        kd = api_data[0].json()
    except Exception as e:
        print e
        return redirect('/')
    spells = api_data[1].json()
    ops = api_data[2].json()
    # build info necessary for nav.html
    kd_links, kdp = navdata(api_data[3].json())

    main = {'kd': kd, 'Type': 'Kingdom', 'kid': kid, 'title': kd['name'] + ' ' + kd['location'],
            'kdb': kd_links, 'kdl': kdl, 'kdp': kdp}

    #Per Acre List
    main['pa'] = ['Peasants', 'Soldiers', 'O Specs', 'D Specs', 'Elites', 'Wizards', 'Thieves', 'Mod Off', 'SoM Off',
                  'Practical Off', 'Max Off', 'Mod Def', 'SoM Def', 'Estimated Def', 'Min Def', 'Elites Home']

    #Dragon
    if kd.get('dragon'):
        main['Dragon'] = kd['dragon']['name']

    # Kingdom Info
    main['name'] = kd['name']
    main['loc'] = kd['location']
    main['kdpage'] = kd

    #Province Data
    main['provinces'], main['totals'] = intel.province_data(kd['provinces'], spells, ops)

    # Intel
    main['intel'] = intel.province_intel(kd['provinces'])

    #SpellOps Header
    main['spellops_header'] = (('Meteor Showers', 'red'), ('Pitfalls', 'yellow'), ('Incite Riots', 'darkorange'), ('Storms', 'purple'),
                               ('Droughts', 'teal'), ('Chastity', 'maroon'), ('Greed', 'green'), ('Explosions', 'brown'),
                               ('Gluttony', 'yellow'), ('Blizzard', 'blue'))

    # #Kingdom Header Stuff
    expire_date = datetime.datetime.now() + datetime.timedelta(days=90)
    kd_pin_expire = datetime.datetime.now() + datetime.timedelta(days=7)

    if 'pin' in request.form.keys():
        main['head'], raw_headers = getKingdomHeader(request.method, 'kd')
        main['kdp'] = (kid, kd['location'])
        resp = make_response(render_template('kingdom.html', data=main))
        resp.set_cookie('pin', str((kid, kd['location'])), expires=kd_pin_expire)

    elif 'reset' in request.form.keys():
        main['head'], raw_headers = getKingdomHeader(request.method, 'kd', default_header=True)
        resp = make_response(render_template('kingdom.html', data=main))
        resp.set_cookie('kd', '', expires=0)

    elif request.method == 'POST':
        main['head'], raw_headers = getKingdomHeader(request.method, 'kd', request.form.keys())
        resp = make_response(render_template('kingdom.html', data=main))
        resp.set_cookie('kd', str(raw_headers), expires=expire_date)

    else:
        main['head'], raw_headers = getKingdomHeader(request.method, 'kd')
        resp = make_response(render_template('kingdom.html', data=main))

    return resp


# Recent Intel
@app.route('/intel/recent', methods=['GET', 'POST'])
@requires_auth
def recentIntel():

    prov_data = fetch(['provinces?ageLimit=96&includeIntel=true'])[0].json()

    provinces = [i for i in prov_data if i['kingdom']['id'] != int(kdl[0]) and i.get('sot')]

    kd = fetch(['kingdoms'])[0].json()
    kd_links, kdp = navdata(kd)

    main = {'Type': 'Intel', 'title': 'Intel', 'kdb': kd_links, 'kdl': kdl, 'kdp': kdp}

    # Province Data
    main['provinces'], main['totals'] = intel.province_data(provinces, spells=[], ops=[])

    # Intel
    main['intel'] = intel.province_intel(provinces)

    # Kingdom Header Stuff
    if request.method == 'POST':
        main['head'], raw_headers = getKingdomHeader(request.method, 'recent_intel', request.form.keys())
        resp = make_response(render_template('kingdom.html', data=main))
        resp.set_cookie('recent_intel', str(raw_headers))
    else:
        main['head'], raw_headers = getKingdomHeader(request.method, 'recent_intel')
        resp = make_response(render_template('kingdom.html', data=main))

    return resp

# Spells/Ops
@app.route('/ops/<int:kid>', methods=['GET', 'POST'])
@requires_auth
def ops(kid):
    api_data = fetch(['kingdoms/' + str(kid), 'provinces?kingdomId=' + str(kid), 'intel/sots?kingdomId=' + str(kid),
          'intel/soss?kingdomId=' + str(kid), 'intel/surveys?kingdomId=' + str(kid), 'armies/?kingdomId=' + str(kid),
          'intel/soms?kingdomId=' + str(kid), 'spells/instant?kingdomIds={}'.format(kid), 'ops/instant?kingdomIds={}'.format(kid), 'kingdoms'])
    #validate that the kd ID trying to be fetched is valid, if not, redirect to homepage.
    try:
        kd = api_data[0].json()
    except Exception as e:
        return redirect('/')

    kd_links, kdp = navdata(api_data[9].json())

    #Build kingdom page data dictionary
    kfetch = api_data[1].json()
    pfetch = api_data[7].json()
    ofetch = api_data[8].json()

    main = {'kd': kd, 'Type': 'Ops', 'kid': kid, 'title': kd['name'] + ' ' + kd['location'], 'kdb': kd_links, 'kdl': kdl, 'kdp': kdp}

    data = []
    totals = defaultdict(int)
    for province in kfetch:
        temp = {}
        temp['province'] = province

         # -> Time
        t = str(datetime.datetime.now() - dateutil.parser.parse(province['lastUpdated'][:-5]))
        t = t[:t.find('.')]
        if 'day' not in t:
            t = '0d ' + t
        province['last'] = t[:t.find(',')]
        temp['Updated'] = province['last']
        temp['Name'] = province['name']
        temp['PID'] = province['id']
        temp['KID'] = province['kingdom']['id']
        temp['KLOC'] = province['kingdom']['location']
        if province.get('owner'):
            temp['Owner'] = province['owner']['name']
        else:
            temp['Owner'] = "NA"

        try:
            temp['NWPA'] = int(province['networth']) / int(province['land'])
            temp['Networth'] = format(province['networth'], ",d")
            temp['Acres'] = format(province['land'], ",d")
            temp['Race'] = province['race']['name']
            temp['Honor'] = province['honorTitle']['name']
            temp['WPA'] = '{:.2f}'.format(float(province['wizards']) / float(province['land']))
            temp['TPA'] = '{:.2f}'.format(float(province['thieves']) / float(province['land']))
            temp['Thieves'] = format(province['thieves'], ",d")
            temp['Estimated Def'] = format(province['estimatedCurrentDefense'], ",d")
            temp['MWPA'] = '{:.2f}'.format(float(province['wizards']) * float(province['wizardryModifiers']) / float(province['land']))
            temp['MTPA'] = '{:.2f}'.format(float(province['thieves']) * float(province['thieveryModifiers']) / float(province['land']))

        except ZeroDivisionError:
            pass
        except Exception:
            app.logger.exception('Error during general section for kd page')

        for spell in pfetch:
            if province['id'] == spell['province']['id']:

                damage = spell['damage']
                spelltype = spell['type']['name']
                temp['spellops'][spelltype] += damage

        for op in ofetch:
            if province['id'] == op['province']['id']:
                damage = op['damage']
                optype = op['type']['name']
                temp['spellops'][optype] += damage

        for key, value in temp['spellops'].iteritems():
            print key
            print value
            totals[key] = (value + int(totals[key]))
            temp[key] = '{0:,}'.format(decimal.Decimal(temp[key]))

        inteltypes = ['province']
        for itype in inteltypes:
            #data formatting
            category = ['land', 'networth']
            for n in category:
                try:
                    temp[itype][n] = '{0:,}'.format(decimal.Decimal(temp[itype][n]))
                except Exception:
                    app.logger.exception('Error during data formatting section for kingdom ops page')

        data.append(temp)

    #Formating the totals
    for key, value in totals.iteritems():
        try:
            totals[key] = '{0:,}'.format(decimal.Decimal(totals[key]))
        except Exception:
            app.logger.exception('Error during total formatting {}'.format(key))

    main['name'] = kd['name']
    main['loc'] = kd['location']
    main['data'] = data
    main['kid'] = kid
    main['totals'] = totals

    #main['head'] = ['Province', 'Race', 'Persona', 'Acres', 'Networth', 'Fireball', 'Nightmare', 'Tornadoes', 'Lighnting Strike', 'Land Lust', 'Night Strike', 'Kidnap', 'Assassinate Wizards', 'Rob Vaults', 'Rob Towers', 'Rob Granaries']

    # Title
    main['title'] = kd['name']+' '+kd['location']
    main['kd'] = kd
    # #Kingdom Header Stuff
    expire_date = datetime.datetime.now() + datetime.timedelta(days=180)

    if 'reset' in request.form.keys():
        main['head'], raw_headers = getSpellOpsHeader(request.method, 'ops', default_header=True)
        resp = make_response(render_template('ops.html', data=main))
        resp.set_cookie('kd', '', expires=0)

    elif request.method == 'POST':
        main['head'], raw_headers = getSpellOpsHeader(request.method, 'ops', request.form.keys())
        resp = make_response(render_template('ops.html', data=main))
        resp.set_cookie('kd', str(raw_headers), expires=expire_date)

    else:
        main['head'], raw_headers = getSpellOpsHeader(request.method, 'ops')
        resp = make_response(render_template('ops.html', data=main))

    return resp


#
#
# SpellOps Province
@app.route('/provinceops/<int:pid>')
@requires_auth
def provinceops(pid):
    api_data = fetch(['provinces/' + str(pid), 'kingdoms'])

    p = api_data[0].json()
    kingdoms = api_data[1].json()

    intel_data = fetch(['intel/sots/' + get_intel_id(p, 'sot'), 'ops/instant?provinceId={}'.format(pid),
                        'spells/instant?provinceId={}'.format(pid)])

    kd_links, kdp = navdata(kingdoms)
    #updated = {}
    main = {'p': p, 'Type': 'Province', 'pid': pid, 'title': p['name'], 'kdb': kd_links, 'kdl': kdl, 'kdp': kdp}

    # try:
    #     main['t'] = intel_data[0].json()
    #     # -> Time
    #     t = str(datetime.datetime.now() - dateutil.parser.parse(main['t']['lastUpdated'][:-5]))
    #     t = t[:t.find('.')]
    #     if 'day' not in t:
    #         updated['sot'] = 'success', t[:t.find(',')]
    #     else:
    #         updated['sot'] = 'danger', t[:t.find(',')]
    # except Exception:
    #     pass

    op_dict = defaultdict(int)
    try:
        for op in intel_data[1].json():
            username = op['caster']['name']
            tries = op['amount']
            damage = op['damage']
            op_dict[op['type']['name']].update({username: [{'tries': tries}, {'damage': damage}]})
    except Exception:
        pass

    spell_dict = defaultdict(int)
    try:
        for spell in intel_data[2].json():
            username = spell['committer']['name']
            tries = spell['amount']
            damage = spell['damage']
            spell_dict[spell['type']['name']].update({username: [{'tries': tries}, {'damage': damage}]})

    except Exception:
        pass

    main['ops'] = op_dict
    main['spells'] = spell_dict
    main['kd_links'] = kd_links

    print main
    return render_template('provinceops.html', data=main)

#
# News
@app.route('/news/<int:kid>')
@requires_auth
def show_news(kid):
    api_data = fetch(['news?kingdomIds={}'.format(kid), 'kingdoms'])

    n = api_data[0].json()
    kingdoms = api_data[1].json()

    kd_links, kdp = navdata(kingdoms)

    main = {'Type': 'News', 'kid': kid, 'kdb': kd_links, 'kdl': kdl, "kdp": kdp}

    main['news'] = n
    main['kd_links'] = kd_links

    return render_template('news.html', data=main)

#
# Ops History
@app.route('/ops_history', methods=['GET', 'POST'])
@requires_auth
def ops_history():

    if request.form.get('kd') and request.form.get('kd') != 'None' and request.form.get('kd') != 'ALL':
        kd_filter = "?kingdomIds={}".format(request.form.get('kd'))
        api_data = fetch(['ops/duration{}'.format(kd_filter), 'ops/instant{}'.format(kd_filter), 'spells/duration{}'.format(kd_filter), 'spells/instant{}'.format(kd_filter), 'kingdoms', 'provinces?kingdomId={}'.format(kdl[0])])
    else:
        api_data = fetch(['ops/duration', 'ops/instant', 'spells/duration', 'spells/instant', 'kingdoms', 'provinces?kingdomId={}'.format(kdl[0])])

    dops = api_data[0].json()
    iops = api_data[1].json()
    dsp = api_data[2].json()
    isp = api_data[3].json()
    skd = api_data[5].json()

    spellop_list = [dops, iops, dsp, isp]
    kingdom_dict = dict()
    province_dict = dict()
    user_dict = dict()
    op_dict = dict()
    self_provinces = list()

    for prov in skd:
        self_provinces.append(prov.get('name'))
    for kd in api_data[4].json():
        kingdom_dict.update({kd.get('location'): kd.get('id')})

    for spellops in spellop_list:
        for spellop in spellops:
            try:
                if spellop.get('committer'):
                    user_dict.update({spellop['committer'].get('name'): spellop['committer'].get('id')})
                else:
                    user_dict.update({spellop['caster'].get('name'): spellop['caster'].get('id')})

                province_dict.update({spellop['province'].get('name'): spellop['province'].get('id')})
                op_dict.update({spellop['type'].get('name'): spellop['type'].get('id')})

            except KeyError as e:
                print e
                print spellop

    kd_links, kdp = navdata(api_data[4].json())
    main = {'Type': 'Ops History', 'kdb': kd_links, 'kdl': kdl, 'kdp': kdp}
    main['head'] = ('Province', 'Op', 'Damage', 'Count', 'User', 'Last Updated')
    main['kingdom_list'] = sorted(kingdom_dict.keys())
    main['province_list'] = sorted(province_dict.keys())
    main['user_list'] = sorted(user_dict.keys())
    main['op_list'] = sorted(op_dict.keys())
    main['kd_links'] = kd_links

    if 'reset' in request.form.keys():
        filters = {'user': '', 'prov': '', 'type': '', 'time': 96}
        main['data'] = filter_spellop_data(spellop_list, filters, self_provinces)
        resp = make_response(render_template('ops_history.html', data=main))

    elif request.method == 'POST':
        main['kd'] = request.form.get('kd')
        main['prov'] = request.form.get('prov')
        main['type'] = request.form.get('type')
        main['user'] = request.form.get('user')
        filters = {'user': request.form['user'], 'prov': request.form['prov'],
                   'type': request.form['type'], 'time': int(request.form['time'])}
        main['data'] = filter_spellop_data(spellop_list, filters, self_provinces)
        resp = make_response(render_template('ops_history.html', data=main))

    else:
        filters = {'user': '', 'prov': '', 'type': '', 'time': 96}
        main['data'] = filter_spellop_data(spellop_list, filters, self_provinces)
        resp = make_response(render_template('ops_history.html', data=main))

    return resp


def filter_spellop_data(spellops_list, filters, self_provinces):

    filtered_data = list()
    for data in spellops_list:
        for spellop in data:
            filtered_data.append(spellop)
    for i in xrange(len(filtered_data) - 1, -1, -1):
        try:
            if filters['user'] and filtered_data[i].get('committer') and filtered_data[i]['committer'].get('name') != filters['user']:
                del filtered_data[i]
                continue
            if filters['user'] and filtered_data[i].get('caster') and filtered_data[i]['caster'].get('name') != filters['user']:
                del filtered_data[i]
                continue
            if filters['prov'] and filtered_data[i]['province'].get('name') != filters['prov']:
                del filtered_data[i]
                continue
            if filters['type'] and filtered_data[i]['type'].get('name') != filters['type']:
                del filtered_data[i]
                continue
            if filtered_data[i]['province'].get('name') in self_provinces:
                del filtered_data[i]
                continue
            if datetime.datetime.utcnow() - datetime.timedelta(hours=filters['time']) > dateutil.parser.parse(filtered_data[i].get('lastUpdated').split("+")[0]):
                del filtered_data[i]
                continue
        except ValueError as e:
            continue

    return filtered_data

#
# Records
@app.route('/records')
@requires_auth
def records():
    api_data = fetch(['record', 'kingdoms'])

    records = api_data[0].json()
    kingdoms = api_data[1].json()

    kd_links, kdp = navdata(kingdoms)
    main = {'Type': 'Records', 'kdb': kd_links, 'kdl': kdl, 'kdp': kdp}

    main['records'] = records
    main['kd_links'] = kd_links
    main['head'] = ('User', 'Type', 'Value', 'All Time')

    return make_response(render_template('records.html', data=main))

#
# Command help
@app.route('/commands')
@requires_auth
def commands():
    api_data = fetch(['help/commands', 'kingdoms'])

    commands = api_data[0].json()
    kingdoms = api_data[1].json()

    kd_links, kdp = navdata(kingdoms)
    main = {'Type': 'Commands', 'kdb': kd_links, 'kdl': kdl, 'kdp': kdp}

    main['commands'] = commands
    main['kd_links'] = kd_links
    main['head'] = ('Name', 'Help Text')

    return make_response(render_template('commands.html', data=main))


#
# User Setup
@app.route('/users')
@requires_auth
def show_users():
    api_data = fetch(['users', 'kingdoms', 'kingdoms/{}'.format(kdl[0])])

    users = api_data[0].json()
    kingdoms = api_data[1].json()
    self_kingdom = api_data[2].json()

    kd_links, kdp = navdata(kingdoms)

    main = {'Type': 'Users', 'kdb': kd_links, 'kdl': kdl, 'kdp': kdp}

    main['users'] = users
    main['provinces'] = dict()
    for prov in self_kingdom['provinces']:
        if prov.get('owner'):
            main['provinces'][prov['owner']['name']] = prov['name']
    main['kd_links'] = kd_links
    main['head'] = ('Name', 'Province', 'Admin', 'Time Zone', 'Real Name', 'SMSEmailAddress')
    # if request.method == 'POST':
    #     session['users'][]
    #     return make_response(render_template('users.html', data=main))
    return make_response(render_template('users.html', data=main))
#
#
# @app.route('/users_update', methods='PUT')
# @requires_auth
# def update_users(kid):
#     api_data = fetch(['users', 'kingdoms', 'provinces?kingdomId={}'.format(kdl), 'kingdoms/{}'.format(kdl)])
#
#     users = api_data[0].json()
#     kingdoms = api_data[1].json()
#     provinces = api_data[2].json()
#     self_kingdom = api_data[3].json()
#
#     kd_links, kdp = navdata(kingdoms)
#
#     main = {'Type': 'Users', 'kid': kid, 'kdb': kd_links, 'kdl': kdl, 'kdp': kdp}
#
#     main['users'] = users
#     main['kd_links'] = kd_links
#     main['provinces'] = self_kingdom.get('provinces')
#
#     return redirect("/users")
