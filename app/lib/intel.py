import datetime
import dateutil.parser
import re
from app import app
import logging
from collections import defaultdict
import decimal
import collections

formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s',
                              datefmt='%Y-%m-%d %H:%M:%S')
fh = logging.FileHandler('error.log')
fh.setFormatter(formatter)
app.logger.addHandler(fh)

SPELLOPS_MAP = {
                'Meteor Showers': 'red',
                'Pitfalls': 'yellow',
                'Incite Riots': 'darkorange',
                'Storms': 'purple',
                'Droughts': 'teal',
                'Chastity': 'maroon',
                'Greed': 'green',
                'Explosions': 'brown',
                'Gluttony': 'yellow',
                'Blizzard': 'blue'
                }


def province_intel(provinces):
    intel = {}

    for province in provinces:
        pi = {}
        inteltypes = ['sot', 'som', 'survey', 'sos', 'infil']
        for itype in inteltypes:
            try:
                if itype == 'infil':
                    t = abs(datetime.datetime.now() - dateutil.parser.parse(province['thievesLastUpdated'][:-5])).total_seconds() / 3600.00
                else:
                    t = abs(datetime.datetime.now() - dateutil.parser.parse(province[itype]['lastUpdated'][:-5])).total_seconds() / 3600.00
                if t <= 6:
                    pi[itype] = '#70E500', "{:.2f} Hours".format(t)
                elif 6 < t <= 12:
                    pi[itype] = '#FF9900', "{:.2f} Hours".format(t)
                elif 12 < t <= 24:
                    pi[itype] = '#990000', "{:.2f} Hours".format(t)
                elif t > 24:
                    pi[itype] = '#FF0000', "{:.2f} Hours".format(t)
            except:
                pi[itype] = 'Black', 'Never'

        intel[province['name']] = pi

    return intel

def province_data(provinces, spells, ops):
    total_list = ('Acres', 'Networth', 'Gold', 'Food', 'Runes', 'Peasants', 'Horses', 'Soldiers', 'O Specs', 'D Specs',
    'Elites', 'Mod Off', 'Mod Def', 'Min Def', 'Max Off', 'Practical Off', 'Estimated Def', 'Spec Credits')
    totals = defaultdict(int)

    formatted_provinces = []
    for province in provinces:
        #adding
        if province.get('networth') and province.get('land)'):
            province['NWPA'] = int(province.get('networth') / int(province.get('land)')))
        else:
            province['NWPA'] = 'NA'


        # -> Time
        t = str(datetime.datetime.now() - dateutil.parser.parse(province['lastUpdated'][:-5]))
        t = t[:t.find('.')]
        if 'day' not in t:
            t = '0d ' + t

        if province.get('land'):
            land = province.get('land')
        else:
            land = 0
        province['last'] = t[:t.find(',')]
        province['Updated'] = province['last']
        province['Name'] = province['name']
        province['PID'] = province['id']
        province['KID'] = province['kingdom']['id']
        province['KLOC'] = province['kingdom']['location']
        if province.get('owner'):
            province['Owner'] = province['owner']['name']
        else:
            province['Owner'] = "NA"

        # Manually adding fields that require calculation or are nested.
        try:
            province['NWPA'] = int(province['networth']) / int(land)
            province['Networth'] = format(province['networth'], ",d")
            province['Acres'] = format(land, ",d")
            province['Race'] = province['race']['name']
            if province.get('honorTitle'):
                province['Honor'] = province['honorTitle']['name']
            else:
                province['Honor'] = ''
            province['WPA'] = '{:.2f}'.format(float(province['wizards']) / float(land))
            province['TPA'] = '{:.2f}'.format(float(province['thieves']) / float(land))
            province['Thieves'] = format(province['thieves'], ",d")
            province['Estimated Def'] = format(province['estimatedCurrentDefense'], ",d")
            province['MWPA'] = '{:.2f}'.format(float(province['wizards']) * float(province['wizardryModifiers']) / float(land))
            province['MTPA'] = '{:.2f}'.format(float(province['thieves']) * float(province['thieveryModifiers']) / float(land))
            province['Wizards'] = format(province['wizards'], ",d")

        except ZeroDivisionError:
            pass
        except Exception:
            app.logger.exception('Error during general section for kd page')
        # SOT
        try:
            if province.get('sot'):
                province['Pers'] = province['personality']['name']
                province['Runes'] = format(province['sot']['runes'], ",d")
                province['Gold'] = format(province['sot']['money'], ",d")
                province['Peasants'] = format(province['sot']['peasants'], ",d")
                province['Soldiers'] = format(province['sot']['soldiers'], ",d")
                province['Mod Off'] = format(province['sot']['modOffense'], ",d")
                province['Mod Def'] = format(province['sot']['modDefense'], ",d")
                province['Max Off'] = format(province['sot']['maxOffense'], ",d")
                province['Min Def'] = format(province['sot']['minDefense'], ",d")
                province['Practical Off'] = format(province['sot']['practicalModOffense'], ",d")
                province['OME'] = "{0:.2f}%".format(float(province['sot']['offensiveME']) * 100)
                province['DME'] = "{0:.2f}%".format(float(province['sot']['defensiveME']) * 100)
                province['D Specs'] = format(province['sot']['defSpecs'], ",d")
                province['Food'] = format(province['sot']['food'], ",d")
                province['BE'] = province['sot']['buildingEfficiency']
                province['O Specs'] = format(province['sot']['offSpecs'], ",d")
                province['Elites'] = format(province['sot']['elites'], ",d")
                province['Horses'] = format(province['sot']['warHorses'], ",d")
                province['PPA'] = int(province['sot']['peasants'] / land)

                if province['sot'].get('hitStatus'):
                    province['Hit'] = province['sot']['hitStatus']
                else:
                    province['Hit'] = ''

        except KeyError or IndexError as e:
            pass
        except Exception:
            app.logger.exception('Error during sot section for kd page')

        # SOM
        try:
            if province.get('som'):
                province['SoM Def'] = format(province['som']['netDefense'], ",d")
                province['SoM Off'] = format(province['som']['netOffense'], ",d")
        except KeyError or IndexError:
            pass
        except Exception:
            app.logger.exception('Error during som section for kd page')
        # Survey
        try:
            if province.get('survey'):
                if not province.get('land'):
                    land = int(province['survey']['built']) + int(province['survey']['inProgress'])
                else:
                    land = province.get('land')
                for entry in province.get('survey')['entries']:
                    if entry['building']['name'] == 'Banks':
                        province['Banks'] = str(int(entry['built'] / (land + 0.0) * 100)) + '%'
                    if entry['building']['name'] == 'Military Barracks':
                        province['Rax'] = str(int(entry['built'] / (land + 0.0) * 100)) + '%'
                    if entry['building']['name'] == 'Watch Towers':
                        province['WT'] = str(int(entry['built'] / (land + 0.0) * 100)) + '%'
                    if entry['building']['name'] == 'Guard Stations':
                        province['GS'] = str(int(entry['built'] / (land + 0.0) * 100)) + '%'
                    if entry['building']['name'] == 'Towers':
                        province['Towers'] = str(int(entry['built'] / (land + 0.0) * 100)) + '%'
                    else:
                        pass
        except KeyError or IndexError:
            pass
        except Exception:
            app.logger.exception('Error during survey section for kd page')

        #SOS
        try:
            if province.get('sos'):
                province['Books'] = format(province['sos']['books'], ",d")
                province['Unallocated Books'] = format(province['sos']['unallocatedBooks'], ",d")
                for entry in province.get('sos')['entries']:
                    province[entry['scienceType']['name']] = format(entry['books'], ",d")

        except KeyError or IndexError as e:
            app.logger.exception(e)
        except Exception:
            app.logger.exception('Error during science section for kd page')

        #Military
        try:
            if province.get('military'):
                province['Spec Creds'] = format(province['military']['specCredits'], ",d")
                province['Wage Rate'] = str(province['military']['wageRate'])
                province['Draft Target'] = str(province['military']['draftTarget']) + '%'
                province['Draft Rate'] = str(province['military']['draftRate']) + '%'

        except KeyError or IndexError as e:
            app.logger.exception(e)
        except Exception:
            app.logger.exception('Error during military section for kd page')

        # Army
        alist = []
        inc_land = 0
        has_army = False
        armies = []
        province['Soonest Army'] = 'NA'
        if province.get('som') and province.get('som').get('armies'):
            for army in province.get('som').get('armies'):
                #Gather relevant armies and return time.
                try:
                    if army.get('returning') and army['type'] == 'Army Out':
                        left = round(abs(dateutil.parser.parse(army['returning'][:-5]) - datetime.datetime.now()).total_seconds() / 3600.00, 2)
                        inc_land += army['gain']
                        has_army = True

                        new_army = army
                        new_army.update({u'left': left})
                        armies.append(new_army)
                        alist.append(left)

                    if army.get('type') == 'Army Training':
                        province['Elites in Training'] = format(army['elites'], ",d")
                        province['OSpecs in Training'] = format(army['offSpecs'], ",d")
                        province['DSpecs in Training'] = format(army['defSpecs'], ",d")

                    if army.get('type') == 'Army Home':
                        province['Elites Home'] = format(army['elites'], ",d")

                except Exception:
                    app.logger.exception('Error during army section for kd page on province {}'.format(province['name']))

        if alist:
            province['Army'] = alist
            province['Inc Land'] = format(inc_land, ',d')
            province['Soonest Army'] = min(alist)
            province['Armies'] = sorted(armies, key=lambda x: x['armyNumber'])

        if not has_army:
            province['Inc Land'] = 'N/A'

        #Calculating the Totals
        for column in total_list:
            try:
                totals[column] += int(province[column].replace(',', ''))
            except Exception:
                pass

        ###########SPELLS AND OPS DICT BUILDER ##############################
        province['spellops'] = {}
        for spell in spells:
            try:
                if province['id'] == spell['province']['id']:
                    try:
                        spell['left'] = abs(dateutil.parser.parse(spell['expires'][:-5]) - datetime.datetime.now()).total_seconds() / 3600.00
                    except Exception:
                        spell['left'] = 0

                    spelltype = spell['type']['name']

                    if spelltype in SPELLOPS_MAP:
                        spell['color'] = SPELLOPS_MAP[spelltype]
                    else:
                        spell['color'] = 'blue'
                    province['spellops'][spelltype] = spell

            except Exception:
                pass

        for op in ops:
            try:
                if province['id'] == op['province']['id']:
                    try:
                        op['left'] = abs(dateutil.parser.parse(op['expires'][:-5]) - datetime.datetime.now()).total_seconds() / 3600.00
                    except Exception:
                        op['left'] = 0

                    optype = op['type']['name']

                    if optype in SPELLOPS_MAP:
                        op['color'] = SPELLOPS_MAP[optype]
                    else:
                        op['color'] = 'blue'
                    province['spellops'][optype] = op

            except Exception:
                pass

        # Finally append the data to the master list
        formatted_provinces.append(province)

    #Formating the totals
    for column in total_list:
        try:
            totals[column] = '{0:,}'.format(decimal.Decimal(totals[column]))
        except Exception:
            app.logger.exception('Error during total formatting {}'.format(column))

    return formatted_provinces, totals
