#!/usr/bin/python
from app import app
import ConfigParser
import os

if __name__ == '__main__':
    settings = ConfigParser.ConfigParser()
    settings.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'app/conf', 'config.cfg'))

    host = settings.get("Settings", 'host')
    debug = settings.getboolean("Settings", 'debug')

    app.run(host=host, debug=debug, port=3000)
